from django.shortcuts import render, HttpResponse
from .models import *
# Create your views here.
def index(request):
    
    estudiantesGrupo1 = Estudiante.objects.filter(grupo=1)
    
    estudiantesGrupo4 = Estudiante.objects.filter(grupo=4)
    
    mismoApellido = Estudiante.objects.filter(apellidos = "Gómez")
    
    mismaEdad = Estudiante.objects.filter(edad=24)
    
    mismoGrupoYapellido = Estudiante.objects.filter(grupo= 3 ,apellidos = "Gómez")
    
    estudiantesGrupo2 = Estudiante.objects.filter(grupo=2)
    estudiantesGrupo3 = Estudiante.objects.filter(grupo=3)
    
    return render(request, 'index.html', {'estudiantesGrupo1':estudiantesGrupo1, 
                                          'estudiantesGrupo4':estudiantesGrupo4, 
                                          'mismoApellido':mismoApellido, 
                                          'mismaEdad':mismaEdad, 
                                          'mismoGrupoYapellido':mismoGrupoYapellido, 
                                          'estudiantesGrupo2':estudiantesGrupo2, 
                                          'estudiantesGrupo3':estudiantesGrupo3})